// const CompressionPlugin = require('compression-webpack-plugin');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// const TerserPlugin = require('terser-webpack-plugin')
//
// module.exports = {
//   runtimeCompiler: true,
//   productionSourceMap: false,
//   configureWebpack: {
//       plugins: [
//           new CompressionPlugin({
//               filename: '[path].br[query]',
//               algorithm: 'brotliCompress',
//               test: /\.(js|css|html|svg)$/,
//               compressionOptions: { level: 11 },
//               threshold: 10240,
//               minRatio: 0.8,
//               cache: true,
//               deleteOriginalAssets: false,
//           }),
//           new TerserPlugin({
//               parallel: true,
//               terserOptions: {
//                   ecma: 6,
//               },
//           }),
//       ],
//   },
//
//   css: {
//     modules: true
//   }
// }
