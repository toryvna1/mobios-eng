const carousel = function (window, document, Math) {
    // --------------------
    // letiables
    // --------------------

    // elements
    let $gallery,
        $spin,
        $works,
        $scroll,
        $space,
        $drag,
        $canvas;

    // projects
    let distance = 0,
        offset = 0,
        current = 0,
        origin = 0,
        originX = 0,
        end = 0,
        start = {},
        work = false,
        scroll = 100,
        /*  высота от обертки к обертке*/
        angle = 133 / 20,
        radius;

    // canvas
    let ctx,
        frame,
        dots = 20,
        data = [],
        center = {};



    // --------------------
    // Utils
    // --------------------

    function random(min, max) {
        return Math.random() * (max - min) + min;
    }

    function convert(n, z) {
        return n / (z / 2000 + 1);
    }

    function opacity(i) {
        return 1 - Math.abs((origin - angle * i) / 20);
    }

    function Dot(x, r, a) {
        this.x = x;
        this.r = r;
        this.a = Math.PI * a + Math.PI / 2;
    }

    function pixels(value) {
        return value * scroll / end
    }

    function degrees(value) {
        return value * end / scroll
    }

    function change(value) {
        offset = value;
        distance = offset;
        current = origin;
    }



    // --------------------
    // Animation
    // --------------------

    function canvas() {
        ctx.clearRect(0, 0, $canvas.width, $canvas.height);
        for (let j = 0; j < data.length; j++) {

            let P = data[j];
            let z = P.r * Math.cos(P.a);
            let s = convert(1, z);
            let x = center.x + convert(P.x, z);
            let y = center.y + convert(P.r * Math.sin(P.a), z);
            let o = -z / P.r * 0.4;

            ctx.fillStyle = 'rgba(255,255,255,' + o + ')';
            ctx.fillRect(x, y, s, s);

            P.a += offset / 2000 + 0.002 * (offset < 0 ? -1 : 1);
            if (P.a > Math.PI * 1.5) {P.a -= Math.PI;}
            if (P.a < Math.PI * 0.5) {P.a += Math.PI;}
        }
    }

    function projects() {
        $spin.style.transform = 'rotateX(' + originX + 'deg)';
        $drag.style.transform = 'translateY(' + origin * 50 / end + 'px)';
        for (let i = 0; i < $works.length; i++) {
            $works[i].style.opacity = opacity(i * 1.05);
        }
    }
    function move() {
        offset *= 0.96;
        originX = (current + distance)/1.11;
        /* 1.126 */
        /*  Если менять мышь то не забыть поменять прозрачность в projects*/
        origin = current + distance - offset ;
    }

    function animate() {
        move();
        projects();
        canvas();
        frame = requestAnimationFrame(animate);
    }



    // --------------------
    // Listeners
    // --------------------

    function wheel(event) {
        let i = event.deltaY > 0 ? 1 : -1;
        let s = angle / 2;
        if (origin > end - s && i > 0) distance = end - origin;
        else if (origin < s && i < 0) distance = origin * i;
        else distance = s * i;
        offset = distance;
        current = origin;
        move();
        return false;
    }

    function resize(e) {
        let width = window.innerWidth;
        if(992 > width && work){
            destroy();
        }
        else if(992 < width && !work) {
            update();
            create();
            init();
        }
        style();

    }

    function destroy() {
        work = false;
        document.removeEventListener('wheel', wheel, false);
        $drag.removeEventListener('mousedown', down, false);
        document.removeEventListener('mousemove', drag, false);
        document.removeEventListener('mouseup', up, false);
    }

    // function click(event) {
    //     change(degrees(event.pageY - this.getBoundingClientRect().top + pixels(origin)));
    // }

    function down(event) {
        event.stopPropagation();
        start.y = event.pageY;
        start.o =  origin;
        change(0);
    }

    function drag(event) {
        if (start.y) {
            let s = event.pageY - start.y;
            let p = pixels(start.o);
            if (p + s > scroll) s = scroll - p;
            if (p + s < 0) s = -p;
            change(degrees(s) + start.o - origin);
        }

    }

    function up() {
        start.y = false;
    }



    // --------------------
    // Creation
    // --------------------

    function create() {
        data.length = 0;
        let width = $canvas.width / 2 / 20;
        let circles = Math.ceil($canvas.width  / 2 / width);
        for (let i = 0; i < circles; i++) {
            let x = i * width;
            let r = random(100, 10);
            for (let j = 0; j < dots; j++) {
                data.push(new Dot(x, r, j / dots));
                i && data.push(new Dot(-x, r, j / dots));
            }
        }
    }

    function style() {
        $gallery.style.transform = 'translateZ(-' + radius + 'px)';
        let rotate = window.innerWidth / 100 * 4 + 10;
        for (let i = 0; i < $works.length; i++) {
            $works[i].style.transform = 'rotateX(-' +  angle * i/100*rotate + 'deg) translateZ(' + radius + 'px)';
        }
    }

    function update() {
        $canvas.width = document.body.offsetWidth;
        $canvas.height = document.body.offsetHeight;
        center.x = $canvas.width / 2;
        center.y = $canvas.height / 2;
        style();
    }



    // --------------------
    // Initialization
    // --------------------

    function values() {
        end = angle * ($works.length - 1);
        ctx = $canvas.getContext('2d');
        radius = $gallery.offsetHeight / 1  / Math.tan(Math.PI / 30);
    }



    function events() {
        document.addEventListener('wheel', wheel, false);
        window.addEventListener('resize', resize, false);
        // $space.addEventListener('mousedown', click, false);
        $drag.addEventListener('mousedown', down, false);
        document.addEventListener('mousemove', drag, false);
        document.addEventListener('mouseup', up, false);
    }

    function elements() {
        $gallery = document.getElementById('gallery');
        $spin = $gallery.firstElementChild;
        $works = $spin.children;
        $scroll = document.getElementById('scroll');
        $space = $scroll.firstElementChild;
        $drag = $space.firstElementChild;
        $canvas = document.querySelector('canvas');
    }

    function init() {
        work = true;
        elements();
        events();
        values();
        resize();
        style();
        animate();

    }
    init();
};

export {carousel};
