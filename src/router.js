import Vue from 'vue'
import Router from 'vue-router'

import MainHomepage from "./views/MainHomepage.vue"

import OurProjects from "./views/OurProjects.vue"
import Services from "./views/Services.vue"
import AboutUs from "./views/AboutUs.vue"
import OurTeam from "./views/OurTeam.vue"
import Contacts from "./views/Contacts.vue"
import WhoAreWe from "./views/WhoAreWe.vue"
import WebDevelopment from "./views/WebDevelopment.vue"
import OnlineShop from "./views/OnlineShop.vue"
import Corporate from "./views/Corporate.vue"
import MotionDesign from "./views/MotionDesign.vue"
import MobileApp from "./views/MobileApp.vue"
import Seo from "./views/Seo.vue"

import Branding from "./views/Branding.vue"
import Context from "./views/Context.vue"


import BusinessSite from "./views/BusinessSite.vue"
import IndividualSite from "./views/IndividualSite.vue"
import VizitkaSite from "./views/VizitkaSite.vue"
import CrowdMarketing from "./views/CrowdMarketing.vue"
import AnalysisCompetitors from "./views/AnalysisCompetitors.vue"
import SeoAudit from "./views/SeoAudit.vue"

// import ContextNew from "./views/ContextNew.vue"
import Brandbook from "./views/Brandbook.vue"

import Smm from "./views/Smm.vue"
import Landing from "./views/Landing.vue"
import Promotion from "./views/Promotion.vue"
import Brief from "./views/Brief.vue"
import BriefSeo from "./views/BriefSeo.vue"
import BriefStyle from "./views/BriefStyle.vue"

import error from "./views/Error.vue"
/////////////////// portfolio
import Ukrticket from "./views/portfolio/Ukrticket.vue"
import Homewood from "./views/portfolio/Homewood.vue"
import PosudGrad from "./views/portfolio/PosudGrad.vue"
import BayBay from "./views/portfolio/BayBay.vue"
import Swisspan from "./views/portfolio/Swisspan.vue"
import YTStar from "./views/portfolio/YTStar.vue"
import ModaBoom from "./views/portfolio/ModaBoom.vue"
import Banzai from "./views/portfolio/Banzai.vue"
import LightTek from "./views/portfolio/lightTek.vue"
import housefit from "./views/portfolio/housefit.vue"
import Tekstilno from "./views/portfolio/Tekstilno.vue"
import Nonna from "./views/portfolio/nonna.vue"
import English from "./views/portfolio/english.vue"
import Yugozapad from "./views/portfolio/yugozapad.vue"
import BankovSpa from "./views/portfolio/bankovSpa.vue"
import Amore from "./views/portfolio/amore.vue"
import Savemax from "./views/portfolio/savemax.vue"
import perevozchik from "./views/portfolio/perevozchik.vue"
import topkolgot from "./views/portfolio/topkolgot.vue"
import bereket from "./views/portfolio/bereket.vue"
import tako from "./views/portfolio/tako.vue"
import housemix from "./views/portfolio/housemix.vue"
import srtekoza from "./views/portfolio/strekoza.vue"
import cosmopolis from "./views/portfolio/cosmopolis.vue"
import doggydogs from "./views/portfolio/doggydogs.vue"
import vaisal from "./views/portfolio/vaisal.vue"
import idealstroy from "./views/portfolio/idealstroy.vue"
import belity from "./views/portfolio/belity.vue"
import emf from "./views/portfolio/emf.vue"
import masterskaya from "./views/portfolio/masterskaya.vue"
import stone from "./views/portfolio/stone.vue"
import boneco from "./views/portfolio/boneco.vue"
import kare from "./views/portfolio/kare.vue"
import kolgotoff from "./views/portfolio/kolgotoff.vue"
import bleachBar from "./views/portfolio/bleachBar.vue"
import minoborona from "./views/portfolio/minoborona.vue"
import prostoPereezd from "./views/portfolio/prostoPereezd.vue"
import cityParking from "./views/portfolio/cityParking.vue"
import lndc from "./views/portfolio/lndc.vue"
import p3dstudio from "./views/portfolio/p3studio.vue"
import lamerica from "./views/portfolio/lamerica.vue"

import ThxPage from "./views/ThxPage.vue"


/////////////////// end of portfolio
import Error from "./views/Error.vue"

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      {
          path: '/',
          component: MainHomepage
      },
      { path: '/portfolio', component: OurProjects },
      { path: '/services', component: Services },
      { path: '/about-us', component: AboutUs },

      { path: '/branding', component: Branding },
      // { path: '/our-team', component: OurTeam },

      { path: '/contacts', component: Contacts },

      // { path: '/who-are-we', component: WhoAreWe },

      { path: '/website-development', component: WebDevelopment },
      { path: '/online-shop', component: OnlineShop },
      { path: '/corporate-site', component: Corporate },
      { path: '/motion-design', component: MotionDesign },
      { path: '/mobile-app', component: MobileApp },
      { path: '/seo-optimization', component: Seo },


      { path: '/business-site', component: BusinessSite },
      { path: '/individual-site', component: IndividualSite },
      { path: '/business-card-website', component: VizitkaSite },
      { path: '/crowd-marketing', component: CrowdMarketing },
      { path: '/analysis-of-competitors', component: AnalysisCompetitors },
      { path: '/seo-audit', component: SeoAudit },

      { path: '/ppc', component: Context },
      { path: '/brandbook', component: Brandbook },

      { path: '/smm', component: Smm },
      { path: '/landing', component: Landing },
      { path: '/promotion', component: Promotion },
      { path: '/brief', component: Brief },
      { path: '/brief-seo', component: BriefSeo },
      { path: '/brief-style', component: BriefStyle },


      { path: '*', component: error },
      // -----------------portfolio------------------------- //
      { path: '/portfolio/ukrticket', component: Ukrticket },
      { path: '/portfolio/homewood', component: Homewood },
      { path: '/portfolio/posudgrad', component: PosudGrad },
      { path: '/portfolio/baybay', component: BayBay },
      { path: '/portfolio/swisspan', component: Swisspan },
      { path: '/portfolio/ytstar', component: YTStar },
      { path: '/portfolio/modaboom', component: ModaBoom },
      { path: '/portfolio/banzai', component: Banzai },
      { path: '/portfolio/lighttek', component: LightTek },
      { path: '/portfolio/housefit', component: housefit },
      { path: '/portfolio/tekstilno', component: Tekstilno },
      { path: '/portfolio/nonna', component: Nonna },
      { path: '/portfolio/english', component: English },
      { path: '/portfolio/yugozapad', component: Yugozapad },
      { path: '/portfolio/bankovspa', component: BankovSpa },
      { path: '/portfolio/amore', component: Amore },
      { path: '/portfolio/savemax', component: Savemax },
      { path: '/portfolio/perevozchik', component: perevozchik },
      { path: '/portfolio/topkolgot', component: topkolgot },
      { path: '/portfolio/bereket', component: bereket },
      { path: '/portfolio/tako', component: tako },
      { path: '/portfolio/housemix', component: housemix },
      { path: '/portfolio/strekoza', component: srtekoza },
      { path: '/portfolio/cosmopolis', component: cosmopolis },
      { path: '/portfolio/doggydogs', component: doggydogs },
      { path: '/portfolio/gvaisal', component: vaisal },
      { path: '/portfolio/idealstroy', component: idealstroy },
      { path: '/portfolio/belity', component: belity },
      { path: '/portfolio/emf', component: emf },
      { path: '/portfolio/masterskaya', component: masterskaya },
      { path: '/portfolio/stone', component: stone },
      { path: '/portfolio/boneco', component: boneco },
      { path: '/portfolio/kare', component: kare },
      { path: '/portfolio/kolgotoff', component: kolgotoff },
      { path: '/portfolio/bleach-bar', component: bleachBar },
      { path: '/portfolio/minoborona', component: minoborona },
      { path: '/portfolio/prosto-pereezd', component: prostoPereezd },
      { path: '/portfolio/city-parking', component: cityParking },
      { path: '/portfolio/lndc', component: lndc },
      { path: '/portfolio/p3dstudio', component: p3dstudio },
      { path: '/portfolio/l-america', component: lamerica },



      // ----------------- end of portfolio------------------------- //
      { path: '/404', component: Error },
      { path: '/thnx', component: ThxPage },

  ]
})


