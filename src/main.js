import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vue2TouchEvents from 'vue2-touch-events'

require('vue-swipe/dist/vue-swipe.css');
import {Swipe, SwipeItem} from 'vue-swipe';
import VueTyperPlugin from 'vue-typer'
import VueNumber from 'vue-number-animation'
import VueTypedJs from 'vue-typed-js'
import Vuex from 'vuex';
import axios from 'axios';
import VueAxios from 'vue-axios'
import store from './store';
import 'animate.css'

import VueWow from 'vue-wow'

import 'vue-threejs/lib/VueThreejs.common.js'

import 'fullpage.js/dist/fullpage.min.css'
import 'fullpage.js/vendors/scrolloverflow.min.js'
import 'fullpage.js/dist/fullpage.js'
// import 'fullpage.js/vendors/scrolloverflow' // Optional. When using scrollOverflow:true
import VueFullPage from 'vue-fullpage.js'
Vue.use(VueFullPage);

import VModal from 'vue-js-modal'
Vue.use(VModal);

import vueHeadful from 'vue-headful';
Vue.component('vue-headful', vueHeadful);

import InputMask from 'vue-input-mask';
Vue.component('input-mask', InputMask);


Vue.use(VueAxios, axios);
Vue.use(Vuex);
Vue.use(VueTypedJs);
Vue.use(VueWow);
Vue.component('swipe', Swipe);
Vue.component('swipe-item', SwipeItem);
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyA0_plrm0SpAhTlg-f_Z9d-AUZZHgNJnsI',
    },
});
Vue.use(VueNumber);
Vue.use(VueTyperPlugin);
Vue.use(Vue2TouchEvents, {
    tapTolerance: 10,
    swipeTolerance: 30,
    longTapTimeInterval: 400
});
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
