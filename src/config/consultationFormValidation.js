const formConsultation = {
    name: {
        value: '',
        placeholder: 'Your name',
        regex: /^[a-zа-я]{3,30}\s?-?\s?[a-zа-я]{0,30}$/i,
        errorMessage: 'Please enter a valid first name',
        hasError: false,
    },
    message: {
        value: '',
        placeholder: 'Your message',
        regex: /^.{20,200}$/i,
        errorMessage: 'Your message should have not less than 20 and not more than 200 symbols in it!',
        hasError: false,
    },
    phone: {
        value: '',
        placeholder: '',
        regex: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12,16}(\s*)?$/i,
        errorMessage: 'Please enter a valid number',
        hasError: false,
    },
};


let alreadySendForm = false;

export {formConsultation, alreadySendForm};