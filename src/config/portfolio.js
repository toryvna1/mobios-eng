﻿//
// При добавлении новых презентаций нужно:
// 1. Указывать презентации в том порядке, в котором они будут отображаться на сайте.


const itemsAll = [
        {
            title: 'Kare',
            description: 'Portal',
            image: require('@/assets/our-projects/kare.png'),
            color: '#951417',
            category: 'portal',
            link: '/portfolio/kare'
        },
        {
            title: 'PosudoGrad',
            description: 'E-commers',
            image: require('@/assets/our-projects/posudograd.jpg'),
            color: '#5fa2eb',
            category: 'e-commerce',
            link: '/portfolio/posudgrad'
        },
        {
            title: 'Ukrticket',
            description: 'Portal',
            image: require('@/assets/our-projects/ukrticket.jpg'),
            color: '#951417',
            category: 'portal',
            link: '/portfolio/ukrticket'
        },
        {
            title: 'idealstroy',
            description: 'Corporate site',
            image: require('@/assets/our-projects/Idealstroy.jpg'),
            color: '#F1C734',
            category: 'corporate site',
            link: '/portfolio/idealstroy'
        },
        {
            title: 'Homewood',
            description: 'Corporate site',
            image: require('@/assets/our-projects/homewood.jpg'),
            color: '#5fd618',
            category: 'corporate site',
            link: '/portfolio/homewood'
        },
        {
            title: 'kolgotoff',
            description: 'E-commers',
            image: require('@/assets/our-projects/kolgotoff.jpg'),
            color: '#fa0067',
            category: 'e-commerce',
            link: '/portfolio/kolgotoff'
        },
        {
            title: 'Swisspan',
            description: 'Corporate site',
            image: require('@/assets/our-projects/swisspan.jpg'),
            color: '#E4892E',
            category: 'corporate site',
            link: '/portfolio/swisspan'
        },
        {
            title: 'BayBay',
            description: 'E-commers',
            image: require('@/assets/our-projects/baybay.jpg'),
            color: '#EA5798',
            category: 'e-commerce',
            link: '/portfolio/baybay'
        },
        {
            title: 'BTC',
            description: 'Corporate site',
            image: require('@/assets/our-projects/btc-front.png'),
            color: '#afd755',
            category: 'corporate site',
            link: '/portfolio/minoborona'
        },
        {
            title: 'cosmopolis',
            description: 'Portal',
            image: require('@/assets/our-projects/Cosmopolis.jpg'),
            color: '#222222',
            category: 'portal',
            link: '/portfolio/cosmopolis'
        },
        {
            title: 'gvaisal',
            description: 'E-commers',
            image: require('@/assets/our-projects/Gvaisal.jpg'),
            color: '#A0DA8B',
            category: 'e-commerce',
            link: '/portfolio/gvaisal'
        },
        {
            title: 'doggydogs',
            description: 'Portal',
            image: require('@/assets/our-projects/DoggyDOG.jpg'),
            color: '#F2CC66',
            category: 'portal',
            link: '/portfolio/doggydogs'
        },
        {
            title: 'ytstar',
            description: 'Portal',
            image: require('@/assets/our-projects/ytstar.jpg'),
            color: '#ff1818',
            category: 'portal',
            link: '/portfolio/ytstar'
        },
        {
            title: 'modaboom',
            description: 'E-commers',
            image: require('@/assets/our-projects/modaboom.jpg'),
            color: '#E5007D',
            category: 'e-commerce',
            link: '/portfolio/modaboom'
        },
        {
            title: 'banzai',
            description: 'Portal',
            image: require('@/assets/our-projects/banzai.jpg'),
            color: '#ee2b28',
            category: 'portal',
            link: '/portfolio/banzai'
        },
        {
            title: 'strekoza',
            description: 'Corporate site',
            image: require('@/assets/our-projects/strekoza.jpg'),
            color: '#4bac5b',
            category: 'corporate site',
            link: '/portfolio/strekoza'
        },
        {
            title: 'lightTek',
            description: 'Corporate site',
            image: require('@/assets/our-projects/laitek.jpg'),
            color: '#FFEB01',
            category: 'corporate site',
            link: '/portfolio/lighttek'
        },
        {
            title: 'housefit',
            description: 'E-commers',
            image: require('@/assets/our-projects/housefit.jpg'),
            color: '#2918d6',
            category: 'e-commerce',
            link: '/portfolio/housefit'
        },
        {
            title: 'tekstilno',
            description: 'E-commers',
            image: require('@/assets/our-projects/tekstilno.jpg'),
            color: '#cd076e',
            category: 'e-commerce',
            link: '/portfolio/tekstilno'
        },
        {
            title: 'bleach bar',
            description: 'Corporate site',
            image: require('@/assets/our-projects/bleechbar.png'),
            color: '#9FC13C',
            category: 'corporate site',
            link: '/portfolio/bleach-bar'
        },
        {
            title: 'nonna',
            description: 'E-commers',
            image: require('@/assets/our-projects/nonna.jpg'),
            color: '#EBD460',
            category: 'e-commerce',
            link: '/portfolio/nonna'
        },
        {
            title: 'YugoZapad',
            description: 'Portal',
            image: require('@/assets/our-projects/yugozapad.jpg'),
            color: '#BF360C',
            category: 'portal',
            link: '/portfolio/yugozapad'
        },
        {
            title: 'Cityparking',
            description: 'E-commers',
            image: require('@/assets/our-projects/cityparking.jpg'),
            color: '#4caf50',
            category: 'e-commerce',
            link: '/portfolio/city-parking'
        },
        {
            title: 'boneco',
            description: 'E-commers',
            image: require('@/assets/our-projects/boneco.jpg'),
            color: '#3a83a3',
            category: 'e-commerce',
            link: '/portfolio/boneco'
        },
        {
            title: 'lndc',
            description: 'E-commers',
            image: require('@/assets/our-projects/lndc.jpg'),
            color: '#72C9E2',
            category: 'e-commerce',
            link: '/portfolio/lndc'
        },
        {
            title: 'p3dstudio',
            description: 'Corporate site',
            image: require('@/assets/our-projects/p3dstudio.jpg'),
            color: '#0097FF',
            category: 'corporate site',
            link: '/portfolio/p3dstudio'
        },
        {
            title: 'bankovspa',
            description: 'Corporate site',
            image: require('@/assets/our-projects/bankov.jpg'),
            color: '#479ab6',
            category: 'corporate site',
            link: '/portfolio/bankovspa'
        },
        {
            title: 'Amore',
            description: 'E-commers',
            image: require('@/assets/our-projects/amore.jpg'),
            color: '#74675F',
            category: 'e-commerce',
            link: '/portfolio/amore'
        },
        {
            title: 'little america',
            description: 'Corporate site',
            image: require('@/assets/our-projects/america.jpg'),
            color: '#3b3b90',
            category: 'corporate site',
            link: '/portfolio/l-america'
        },

        {
            title: 'stone-bridge',
            description: 'Corporate site',
            image: require('@/assets/our-projects/stone.jpg'),
            color: '#7d0302',
            category: 'corporate site',
            link: '/portfolio/stone'
        },
        {
            title: 'Savemax',
            description: 'Landing page',
            image: require('@/assets/our-projects/savemax.jpg'),
            color: '#CB0000',
            category: 'landing page',
            link: '/portfolio/savemax'
        },
        {
            title: 'masterskaya',
            description: 'Corporate site',
            image: require('@/assets/our-projects/masterskaya.jpg'),
            color: '#b7b2d0',
            category: 'corporate site',
            link: '/portfolio/masterskaya'
        },
        {
            title: 'Perevozchik',
            description: 'Corporate site',
            image: require('@/assets/our-projects/perevozchik.jpg'),
            color: '#d66418',
            category: 'corporate site',
            link: '/portfolio/perevozchik'
        },
        {
            title: 'emf',
            description: 'Corporate site',
            image: require('@/assets/our-projects/emf.jpg'),
            color: '#f04b35',
            category: 'corporate site',
            link: '/portfolio/emf'
        },
        {
            title: 'belity',
            description: 'E-commers',
            image: require('@/assets/our-projects/belity.jpg'),
            color: '#ffa800',
            category: 'e-commerce',
            link: '/portfolio/belity'
        },
        {
            title: 'top-kolgot',
            description: 'E-commers',
            image: require('@/assets/our-projects/topkolgot.jpg'),
            color: '#ffa6b7',
            category: 'e-commerce',
            link: '/portfolio/topkolgot'
        },
        {
            title: 'bereket',
            description: 'E-commers',
            image: require('@/assets/our-projects/bereket.jpg'),
            color: '#DE656D',
            category: 'e-commerce',
            link: '/portfolio/bereket'
        },
        {
            title: 'tako',
            description: 'E-commers',
            image: require('@/assets/our-projects/tako.jpg'),
            color: '#df596d',
            category: 'e-commerce',
            link: '/portfolio/tako'
        },
        {
            title: 'housemix',
            description: 'E-commers',
            image: require('@/assets/our-projects/housemix.jpg'),
            color: '#BFAD9F',
            category: 'e-commerce',
            link: '/portfolio/housemix'
        },
        {
            title: 'English School',
            description: 'Corporate site',
            image: require('@/assets/our-projects/english.jpg'),
            color: '#479ab6',
            category: 'corporate site',
            link: '/portfolio/english'
        },
]

export default itemsAll;