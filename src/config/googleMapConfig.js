const center = {
    lat: 46.458551,
    lng: 30.740516
};
const styles = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#f5f5f5"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#bdbdbd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e5e5e5"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dadada"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e5e5e5"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c9c9c9"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    }
];
const markers = [
    {
        id: 1,
        /*Canada*/

        position: {
            lat: 43.808859,
            lng: -79.257221
        },
        icon: {
            url: require("../assets/marker.svg"),
            scaledSize: {
                height: 30,
                width: 30,
            },
            labelOrigin: {
                x: 20,
                y: 50,
                width: 20,
            },
        },
        label: {
            text: 'MOBIOS STUDIO',
            color: "#00b2d9",
            fontSize: "24px",
            fontWeight: "900",
            fontFamily: 'Geometria',
        }
    },
    {
        id: 2,
        /*Odessa*/
        position: {
            lat: 46.458551,
            lng: 30.740516
        },
        icon: {
            url: require("../assets/marker.svg"),
            scaledSize: {
                height: 30,
                width: 30,
            },
            labelOrigin: {
                x: 20,
                y: 50,
            },
        },
        label: {
            text: 'MOBIOS STUDIO',
            color: "#00b2d9",
            fontSize: "24px",
            fontWeight: "900",
            fontFamily: 'Geometria',
        }
        ,
    },
    {
        id: 3,
        /*Kiev*/
        position: {
            lat: 50.448687,
            lng: 30.532373
        },
        icon: {
            url: require("../assets/marker.svg"),
            scaledSize: {
                height: 30,
                width: 30,
            },
            labelOrigin: {
                x: 20,
                y: 50,
            },
        },
        label: {
            text: 'MOBIOS STUDIO',
            color: "#00b2d9",
            fontSize: "24px",
            fontWeight: "900",
            fontFamily: 'Geometria',
        },
    },
];
export {center, styles, markers}
