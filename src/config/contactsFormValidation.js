const form = {
    name: {
        value: '',
        placeholder: 'Your name',
        regex: /^[a-zа-я]{3,30}\s?-?\s?[a-zа-я]{0,30}$/i,
        errorMessage: 'Please enter a valid first name',
        hasError: false,
    },
    phone: {
        value: '',
        placeholder: '',
        regex: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12,16}(\s*)?$/i,
        errorMessage: 'Please enter a valid number',
        hasError: false,
    },
    email: {
        value: '',
        placeholder: 'Your email',
        regex: /^[a-z0-9/.!#$%&'*+-/=?^_`{|}~]{2,30}@[a-z0-9/-]{2,10}\.[a-z0-9/-]{2,10}$/i,
        errorMessage: 'Please enter a valid e-mail',
        hasError: false,
    },
    message: {
        value: '',
        placeholder: 'Your message',
        regex: /^.{20,200}$/i,
        errorMessage: 'Your message should have not less than 20 and not more than 200 symbols in it!',
        hasError: false,
    },
};


let alreadySend = false;

export {form, alreadySend};