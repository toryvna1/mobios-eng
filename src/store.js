import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        lineLoading: false
    },
    mutations: {
        lineLoading(state){
            state.lineLoading = !state.lineLoading;
        }
    },
    getters: {
        LINE(state){
            return state.lineLoading;
        },
    },
    actions: {
        line({commit,state}){
            state.lineLoading = true;
            setTimeout(()=>{state.lineLoading = false},1500)
        }
    }
});
